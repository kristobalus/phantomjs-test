
const sm = require('sitemap');
const _ = require('lodash');
const request = require('request-promise');
const path = require('path');
const mkdirp = require('mkdirp');
const childProcess = require('child_process');
const phantomjs = require('phantomjs-prebuilt');
const binPath = phantomjs.path;
const fs = require('fs');
const cheerio = require('cheerio');
const { URL } = require('url');

const everlastToken = "554460982005acfb323fde427fe5b3a0b465ee2c33d535bc4facb98183fb527a";
const menuProductUrl = "https://backend.halykbank.kz/struct/categories?parentName=PRODUCTS";
const menuAboutUrl = "https://backend.halykbank.kz/struct/categories?name=ABOUT";
const rootPageUrl = "https://www.halykbank.kz/";


async function downloadFile(url) {

    if (!url) {
        return;
    }

    console.log("downloading...", url);
    url = url.replace(/\?(.*)$/, "");

    let parts = path.parse(url);
    let output = "output" + parts.dir + "/" + parts.base;;
    if (url.indexOf("http://") == -1 && url.indexOf("https://") == -1) {
        url = rootPageUrl + url;
    }
    mkdirp.sync("output" + parts.dir);

    // console.log(parts);
    // console.log(output);
    // console.log(url);

    return new Promise((resolve, reject) => {
        request
            .get(url)
            .on('error', (err) => {
                if (err)
                    console.error(err)
                resolve();
            })
            .pipe(fs.createWriteStream(output))
            .on('close', (err) => {
                if (err)
                    console.error(err);
                resolve();
            });
    })
}

async function fetchPage(pageUrl, location) {

    let result = await new Promise((resolve, reject) => {

        const childArgs = [            
            '--config=' + path.join(__dirname, '..', 'phantomjs', 'config.json'),
            path.join(__dirname, '..', 'phantomjs', 'fetcher.js'), // 0
            pageUrl, // 1
            location,  // 2        
        ]

        console.log(binPath, childArgs);

        childProcess.execFile(binPath, childArgs, function (err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
            if (err)
                return reject(err);
            resolve();
        });
    });

    return result;
}

async function getAssets(location) {

    let html = fs.readFileSync('output/' + location + '.html');
    const $ = cheerio.load(html.toString());

    const urls = [];
    $('a').each(function (i, elem) {
        let url = $(this).attr('href');
        if (url) {
            urls[i] = url;
        }
    });
    console.log("<a> tags", urls);

    const assets = [];
    $('link').each(function (i, elem) {
        let url = $(this).attr('href');
        if (url) {
            assets[i] = url;
        }
    });
    console.log("<link> tags", assets);
    for (let url of assets) {
        await downloadFile(url);
    }

    const scripts = [];
    $('script').each(function (i, elem) {
        let url = $(this).attr('src');
        if (url) {
            if (url.indexOf("https:") == -1 && url.indexOf("http:") == -1) {
                scripts[i] = url;
            }
        }
    });
    console.log("<script> tags", scripts);
    for (let url of scripts) {
        await downloadFile(url);
    }
}

async function invalidateAssets(location) {

    let html = fs.readFileSync('output/' + location + '.html');    
    let result = html.toString('utf8')
                    .replace(/"\/assets/g, '"/_assets')
                    .replace(/"\/bundles/g, '"/_bundles');

    fs.writeFileSync('output/' + location + '.html', result, 'utf8');    
}

async function getAuth() {

    let response = await request({
        uri: "https://backend.halykbank.kz/auth/anonymous?appKey=0123456789ABCDEFGHIGKLMN",
        json: true
    });

    const auth = response.data.auth;
    console.log(auth.userId);
    console.log(auth.token);
    console.log(auth.time);

    return auth;
}

function getUrl(node) {
    let result = [];
    if (node.type == "pages") {
        if (node.inscribing == "outlet") {
            if (!node.component.match(/^info\-section/)) {
                result.push(node.url);
            }
        }
    }
    if (node.inscribing == "outlet") {
        if (node.children) {
            for (let child of node.children) {
                result.push(...getUrl(child));
            }
        }
    }
    return result;
}

async function getProducts(depth) {

    let response = await request({
        uri: menuProductUrl,
        json: true,
        qs: { depth },
        rejectUnauthorized: false,
        headers: {
            "Auth-Token": everlastToken
        }
    });

    return response;
}

async function getAbout(depth) {

    let response = await request({
        uri: menuAboutUrl,
        json: true,
        qs: { depth },
        rejectUnauthorized: false,
        headers: {
            "Auth-Token": everlastToken
        }
    });

    return response;
}

async function sitemap(urlList) {

    
    const options = {
        hostname: 'https://halykbank.kz',
        cacheTime: 600000 // 600 sec - cache purge period
    };

    const sitemap = sm.createSitemap({ options });
    for (let url of urlList) {
        sitemap.add({ url: rootPageUrl + url, changefreq: 'monthly' });
    }

    // Generates XML with a callback function 
    let xml = await new Promise((resolve, reject) => {
        sitemap.toXML(function (err, xml) {
            if (err)
                return reject(err);
            console.log(xml);
            resolve(xml);
        });
    });

    console.log(xml);

    // Gives you a string containing the XML data 
    xml = sitemap.toString();
    console.log(xml);

    fs.writeFileSync('output/sitemap.xml', xml);
    // throw new Error("Stop this!");
}

async function main() {

    // let auth = await getAuth();
    // console.log(auth);
    // await fetchPage("https://www.halykbank.kz", "page-one");
    // console.log('Page fetched.');

    let pageUrls = [];
    let products = [];

    try {
        products = await getProducts(3);
    } catch(err){
        console.log(err);
        process.exit(0);
    }
    
    for (let node of products.data) {
        pageUrls.push(...getUrl(node));
    }

    products = await getProducts(4);
    for (let node of products.data) {
        pageUrls.push(...getUrl(node));
    }

    let about = [];

    about = await getAbout(3);
    for (let node of about.data) {
        pageUrls.push(...getUrl(node));
    }

    about = await getAbout(4);
    for (let node of about.data) {
        pageUrls.push(...getUrl(node));
    }

    let fetchPaths = [];
    for (let url of pageUrls) {
        fetchPaths.push(url.replace("/halykbank_root/glavnaya_stranica/", ""));
    }

    await sitemap(fetchPaths);

    console.log(fetchPaths);
    console.log(`total:${fetchPaths.length}`);

    console.log("loading root page...");
    try {
        await fetchPage(rootPageUrl, "index");
        await invalidateAssets('index');
    } catch (err) {
        console.error(err);
    }
    console.log("root page loaded");

    for (let location of fetchPaths) {
        let parts = location.split("/");
        let filename = parts.pop();
        if (parts.length > 0) {
            try {
                parts.unshift("output");
                mkdirp.sync(parts.join("/"));
            } catch (err) {
                // dir exists!
            }
        }

        let url = rootPageUrl + location;
        console.log(`Fetching ${url} to location ${location}...`);

        try {
            await fetchPage(url, location);
            await invalidateAssets(location);
        } catch (err) {
            console.error(err);
        }

        console.log(`Location ${location} fetched.`);
    }

    console.log("Bye.");
}

main();


