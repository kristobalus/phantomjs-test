/**
 * Wait until the test condition is true or a timeout occurs. Useful for waiting
 * on a server response or for a ui change (fadeIn, etc.) to occur.
 *
 * @param testFx javascript condition that evaluates to a boolean,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param onReady what to do when testFx condition is fulfilled,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param timeOutMillis the max amount of time to wait. If not specified, 3 sec is used.
 * "use strict";
 */

function waitFor(testFx, onReady, timeOutMillis) {

    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 60000, //< Default Max Timout is 60s
        start = new Date().getTime(),
        condition = false,

        interval = setInterval(function () {
            if ((new Date().getTime() - start < maxtimeOutMillis) && !condition) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof (testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {

                if (!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.error("'waitFor()' timeout");
                    phantom.exit(0);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof (onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 250); //< repeat check every 250ms
};


phantom.onError = function (msg, trace) {
    console.log(msg);
    console.log(JSON.stringify(trace));
    console.trace();
    var msgStack = ['PHANTOM ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function (t) {
            msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function + ')' : ''));
        });
    }
    console.log(msgStack.join('\n'));
    // phantom.exit(1);
};

var fs = require('fs');
var page = require('webpage').create();
var isPageReady = false;

page.onConsoleMessage = function (msg, lineNum, sourceId) {
    // this.windowRef.nativeWindow.prerenderReady = true
    if (msg == "this.windowRef.nativeWindow.prerenderReady = true") {
        isPageReady = true;
    }
    console.log('CONSOLE: --' + msg + '-- (from line #' + lineNum + ' in "' + sourceId + '")');
};

page.onError = function (msg, trace) {

    var msgStack = ['ERROR: ' + msg];

    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function (t) {
            msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
        });
    }
    
    console.log(msg);
    console.log(trace);
    console.error(msgStack.join('\n'));
};

page.onResourceError = function(resourceError) {
    console.log('Unable to load resource (#' + resourceError.id + 'URL:' + resourceError.url + ')');
    console.log('Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString);
};
  
page.onAlert = function (msg) {
    console.log("Caught on Alert: " + msg);
};

page.viewportSize = {
    width: 1920,
    height: 1080
};


var args = require('system').args;
var url = args[1];
var outputFilename = args[2];

page.open(url, function (status) {
    // Check for page load success
    if (status !== "success") {
        console.log("Unable to access network");
        console.log(args);
        console.log(url, outputFilename);
        phantom.exit(1);
    } else {
        console.log("Html body loaded. Waiting for finalization signal...");
        waitFor(function () {
            return isPageReady;
        }, function () {
            console.log("Page fully loaded.");
            // console.log(page.content);            
            page.render('output/' + outputFilename + '.png');
            fs.write('output/'+ outputFilename + '.html', page.content, 'w');
            phantom.exit();
        });
    }
});